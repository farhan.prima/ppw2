from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='index'),
    path('story1/', views.story1, name='story1'),
    path('wireframe/', views.wireframe, name='wireframe'),
    path('subscribe/', views.subscribe, name='subscribe'),
]