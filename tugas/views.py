from django.shortcuts import render,redirect

# Create your views here.
def home(request):
    return render(request,'pages/index.html',{})

# Create your views here.
def story1(request):
    return render(request,'pages/index_s1.html',{})

# Create your views here.
def wireframe(request):
    return render(request,'pages/wireframe.html',{})

# Create your views here.
def subscribe(request):
    return render(request,'pages/subscribe.html',{})